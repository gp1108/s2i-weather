const path = require('path');
const Dotenv = require('dotenv-webpack');

const indexConfig = {
  entry: './public/scripts/index.js',
  plugins: [
    new Dotenv(),
  ],
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'index.js',
  },
  devServer: {
    contentBase: './public/',
    port: 8000,
  },
  watchOptions: {
    poll: 500,
  },
};

const cityConfig = {
  entry: './public/scripts/city.js',
  output: {
    path: path.resolve(__dirname, 'public/dist'),
    filename: 'city.js',
  },
  plugins: [
    new Dotenv(),
  ],
};

module.exports = [
  indexConfig, cityConfig,
];
