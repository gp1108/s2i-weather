# S2I Pollution

To correctly use the project follow this steps:

PREREQUISITES:

-npm installed onto the machine

-personal API key for the AQICN API (retrievable here: https://aqicn.org/data-platform/token/#/)




INSTALLATION:

-place yourself in the main project folder

-create a copy of the file ".env.example"

-rename that copy to ".env"

-edit the file as suggested into the file itself

-execute "npm install"

-execute "npm run build"

-excute "npm run server"

-go to the link provided by the terminal with your browser

-enjoy




NOTE:
The webpack dev server doesn't reload upon changes, no matter what i tried (or where i asked) it seems to me that i can't make it work.