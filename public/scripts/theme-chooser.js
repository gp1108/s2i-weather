// GLOBALS
let firstTime = true;

// DEFINING DOM OBJECTS
const themeButton = document.getElementById('theme-button');
const smallThemeButton = document.getElementById('theme-button-sm');
const themeMenu = document.querySelector('.theme-menu');
const sandButton = document.getElementById('sand');
const forestButton = document.getElementById('forest');
const seaButton = document.getElementById('sea');

// DEFINING FUNCTIONS

function displayThemeMenu(event) {
  const position = {
    top: event.target.offsetTop,
    left: event.target.offsetLeft,
  };

  const size = {
    height: event.target.offsetHeight,
    width: event.target.offsetWidth,
  };
  themeMenu.style.top = `${position.top + size.height}px`;
  themeMenu.style.left = `${position.left + size.width / 2 - 45}px`;
  themeMenu.classList.toggle('d-none');
}

function changeTheme(event) {
  const links = document.getElementsByTagName('link');
  let i = 0;
  for (i = 0; i < links.length; i += 1) {
    if (links[i].href.includes('theme-')) {
      break;
    }
  }

  const oldTheme = document.querySelector('.theme-selected');
  oldTheme.classList.toggle('theme-selected');
  event.target.classList.toggle('theme-selected');

  const themeName = event.target.id;
  localStorage.setItem('theme', themeName);
  links[i].href = `./styles/theme-${themeName}.css`;

  const images = document.querySelectorAll('img');
  for (let j = 0; j < images.length; j += 1) {
    const source = images[j].src;
    let score = 0;
    // eslint-disable-next-line for-direction
    for (score = source.length - 1; score < source.length; score -= 1) {
      if (source[score] === '-') { break; }
    }
    const newSource = `${source.slice(0, score)}-${themeName}.png`;
    images[j].src = newSource;
  }

  if (!firstTime) {
    themeMenu.classList.toggle('d-none');
  } else {
    firstTime = false;
  }
}

// ADD EVENT LISTENERS
themeButton.addEventListener('click', displayThemeMenu);
smallThemeButton.addEventListener('click', displayThemeMenu);
sandButton.addEventListener('click', changeTheme);
forestButton.addEventListener('click', changeTheme);
seaButton.addEventListener('click', changeTheme);

if (localStorage.getItem('theme')) {
  switch (localStorage.getItem('theme')) {
    case 'sand':
      break;
    case 'sea':
      seaButton.click();
      break;
    case 'forest':
      forestButton.click();
      break;
    default:
      sandButton.click();
      break;
  }
} else {
  localStorage.setItem('theme', 'sand');
}
