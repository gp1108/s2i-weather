/* eslint-disable import/extensions */

'use-strict';

// IMPORTING

import './search.js';
import './theme-chooser.js';

// ACQUIRING CITY

let city = sessionStorage.getItem('city');
city = JSON.parse(city);

// ACQUIRING DOM OBJECTS

let mainCard = document.querySelector('div.row.mx-3');
mainCard = mainCard.querySelector('.col.footer');
const cityH1 = mainCard.querySelector('h1');
const strongs = mainCard.querySelectorAll('p>strong:not(.no-mod)');
const link = mainCard.querySelector('a');

// FUNCTIONS

function changeTitle() {
  const title = document.querySelector('title');
  title.innerHTML = city.data.city.name;
}

function fillInfo() {
  cityH1.innerHTML = city.data.city.name;

  const airIndex = city.data.aqi;
  let situation = '';
  if (airIndex <= 50) {
    situation = 'good';
  } else if (airIndex <= 100) {
    situation = 'moderate';
  } else if (airIndex <= 150) {
    situation = 'unhealty for sensitive groups';
  } else if (airIndex <= 200) {
    situation = 'unhealthy';
  } else if (airIndex <= 300) {
    situation = 'very unhealthy';
  } else {
    situation = 'hazardous';
  }
  strongs[0].innerHTML = situation;
  strongs[1].innerHTML = city.data.iaqi.pm25.v;
  strongs[2].innerHTML = airIndex;

  link.href = city.data.city.url;
}

// TO BE RUNNED

changeTitle();
fillInfo();
