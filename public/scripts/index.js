/* eslint-disable import/extensions */

'use-strict';

import * as Search from './search.js';
import './theme-chooser.js';

function fillCard(cityString, card) {
  const header = card.querySelector('h5');
  const paragraph = card.querySelector('p');
  card.classList.toggle('d-none');
  const response = Search.stringSearch(cityString);
  response.then(
    (resolve) => {
      header.innerHTML = cityString;
      let situation = '';
      if (response) {
        const airIndex = resolve.data.aqi;
        if (airIndex <= 50) {
          situation = 'good';
        } else if (airIndex <= 100) {
          situation = 'moderate';
        } else if (airIndex <= 150) {
          situation = 'unhealty for sensitive groups';
        } else if (airIndex <= 200) {
          situation = 'unhealthy';
        } else if (airIndex <= 300) {
          situation = 'very unhealthy';
        } else {
          situation = 'hazardous';
        }
      }
      if (situation !== '') {
        paragraph.innerHTML = `The situation is ${situation}.<br>Click for more info!`;
      } else {
        paragraph.innerHTML = 'Click fore more info!';
      }

      const cardClickable = card.querySelector('div.city-card');
      cardClickable.addEventListener('click', () => {
        Search.validation(resolve, Search.mainInput);
      });
    },
    () => {
      paragraph.innerHTML = 'Click for more info!';
    },
  );
}

function showRecent() {
  const no1 = localStorage.getItem('no1');
  const no2 = localStorage.getItem('no2');
  const no3 = localStorage.getItem('no3');

  const card1 = document.getElementById('card1');
  const card2 = document.getElementById('card2');
  const card3 = document.getElementById('card3');

  const alreadyInserted = [];

  if (no1 && !alreadyInserted.includes(no1)) {
    fillCard(no1, card1);
    alreadyInserted.push(no1);
  }
  if (no2 && !alreadyInserted.includes(no2)) {
    fillCard(no2, card2);
    alreadyInserted.push(no2);
  }
  if (no3 && !alreadyInserted.includes(no3)) {
    fillCard(no3, card3);
    alreadyInserted.push(no3);
  }
}

// TO BE RUNNED

showRecent();
