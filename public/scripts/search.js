'use-strict';

// DOM SELECTION
const smallInput = document.querySelector('.smallSearch');
export const mainInput = document.querySelector('.mainSearch');

const candidates = document.querySelectorAll('img.button.logo.searchButton');
let searchBtn;
for (let i = 0; i < candidates.length; i += 1) {
  if (candidates[i].src.includes('search')) {
    searchBtn = candidates[i];
  }
}

// FUNCTIONS

export async function stringSearch(cityName) {
  // console.log(`https://api.waqi.info/feed/${cityName}/?token=${process.env.API_KEY}`);
  const response = await fetch(`https://api.waqi.info/feed/${cityName}/?token=${process.env.API_KEY}`);
  const jsonObj = await response.json();
  return jsonObj;
}

async function geoSearch(geoPosition) {
  // console.log(`https://api.waqi.info/feed/geo:${geoPosition.coords.latitude};${geoPosition.coords.longitude}/?token=${process.env.API_KEY}`);
  const response = await fetch(`https://api.waqi.info/feed/geo:${geoPosition.coords.latitude};${geoPosition.coords.longitude}/?token=${process.env.API_KEY}`);
  const jsonObj = await response.json();
  return jsonObj;
}

function recentlyVisited(cityString) {
  const no1 = localStorage.getItem('no1');
  const no2 = localStorage.getItem('no2');
  if (no1 && no2) {
    localStorage.setItem('no1', cityString);
    localStorage.setItem('no2', no1);
    localStorage.setItem('no3', no2);
  } else if (no1) {
    localStorage.setItem('no1', cityString);
    localStorage.setItem('no2', no1);
  } else {
    localStorage.setItem('no1', cityString);
  }
}

export function validation(response, inputField) {
  const input = inputField;
  if (response.status && response.status === 'ok') {
    sessionStorage.setItem('city', JSON.stringify(response));
    recentlyVisited(response.data.city.name);
    window.location.replace('./city.html');
  } else {
    input.classList.add('apply-shake');
    input.value = '';
    setTimeout(() => {
      inputField.classList.remove('apply-shake');
    }, 1000);
  }
}

function enterKey(event) {
  if (event.keyCode === 13) {
    if (event.target === smallInput) {
      searchBtn.click();
    } else {
      const response = stringSearch(mainInput.value);
      response.then((resolve) => {
        validation(resolve, mainInput);
      },
      (error) => {
        alert('There was an error');
        console.log(error);
      });
    }
  }
}

// ADDING EVENT LISTENERS
mainInput.addEventListener('keydown', enterKey);
smallInput.addEventListener('keydown', enterKey);

const locationBtn = document.querySelector('.location');
locationBtn.addEventListener('click', () => {
  navigator.geolocation.getCurrentPosition(
    (success) => {
      const response = geoSearch(success);
      response.then(
        (resolve) => {
          validation(resolve, mainInput);
        },
        (error) => {
          alert('Can\'t get current location');
          console.log(error);
        },
      );
    },
    (error) => {
      alert('not acquired position');
      console.log(error);
    },
  );
});

searchBtn.addEventListener('click', () => {
  const response = stringSearch(smallInput.value);
  response.then((resolve) => {
    validation(resolve, smallInput);
  },
  (error) => {
    alert('An error has occured!');
    console.log(error);
  });
});
